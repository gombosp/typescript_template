import { IApp } from './modules/app';

export interface IStore {
    app: IApp;
    intl: any;
}