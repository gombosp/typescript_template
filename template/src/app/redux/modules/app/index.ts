export const TEXT_FROM_INPUTBOX: string = 'TEXT_FROM_INPUTBOX';
export const TEXT_TO_STORE: string = 'TEXT_TO_STORE';
import produce from 'immer';
import { action } from 'typesafe-actions';

export interface IApp {
    textFromInputBox: string;
}

const initalizeState: IApp = {
    textFromInputBox: 'basictext',
};

export const actions = {
    textInputBox: (id: string) => action(TEXT_TO_STORE, id ),
}

export default (state = initalizeState, action: {type: string, payload: string}) => 
    produce(state, draft => {
        switch(action.type) {
            case TEXT_FROM_INPUTBOX:
                draft.textFromInputBox = action.payload;
                return draft;
            default:
                return state;
        }
});