import { TEXT_FROM_INPUTBOX } from '../modules/app';
import reducer from '../modules/app';

describe('app reducer test', () => {
    it('it should change textFromInputBox', () => {
        expect(reducer(undefined, {type: TEXT_FROM_INPUTBOX, payload: 'something'})).toEqual(
            {
                textFromInputBox: 'something'
            }
        );
    });
});