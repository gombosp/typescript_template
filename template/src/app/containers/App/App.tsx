import * as React from 'react';
import './style.css';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { injectIntl } from 'react-intl';
import { updateIntl } from 'react-intl-redux';
import { translationMessages } from '../../trans/translator';
import logo from '../../assets/logo.svg';
import { actions } from '../../redux/modules/app'

class App extends React.Component<any> {
  public handleChange = (event: any) => {
    const text = event.target.value;
    this.props.next(text);
  }

  public handleSubmit = () => {
    alert('InputBox value from Store: ' + this.props.app.textFromInputBox)
  }

  public handleLanguageChange = () => {
    const lang = 'hu';
    this.props.updateIntl({
      locale: lang,
      messages: translationMessages[lang],
    });
  }

  public handleLanguageChangeEn = () => {
    const lang = 'en';
    this.props.updateIntl({
      locale: lang,
      messages: translationMessages[lang],
    });
  }
  
  public render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.tsx</code> and save to reload.
          <br />
          {this.props.intl.formatMessage({id: '_app.homemodalbody'})}
        </p>
        <div>
          <input type="text" value={this.props.app.textFromInputBox} onChange={this.handleChange} />
          <input type="submit" onClick={this.handleSubmit} />
          <br />
          <input type="submit" value="Magyar" onClick={this.handleLanguageChange} />
          <br />
          <input type="submit" value="Angol" onClick={this.handleLanguageChangeEn} />
        </div>

      </div>
    );
  }
}

const mapStateToProps = (state: any, props: any) => {
  return {
    app: state.app,
    ...props
  }
};

const mapDispatchToProps = (dispatch: any) => {
  return bindActionCreators({
    next: actions.textInputBox,
    updateIntl,
  }, dispatch);
}

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(App));